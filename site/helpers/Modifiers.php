<?php


namespace Statamic\SiteHelpers;

use Statamic\API\YAML;


class Modifiers
{
    /**
     * Maps to {{ var | example }} or {{ var example="..." }}
     *
     * @param mixed  $value    The value to be modified
     * @param array  $params   Any parameters used in the modifier
     * @param array  $context  Contextual values
     * @return mixed
     */
    public function example($value, $params, $context)
    {
        //
    }

    /**
     * Maps to {{ var | to_yaml }} or {{ var to_yaml="..." }}
     *
     * @param mixed  $value    The value to be modified
     * @param array  $params   Any parameters used in the modifier
     * @param array  $context  Contextual values
     * @return mixed
     */
    public function toYaml($value, $params, $context)
    {

        $html = '';
        $data = html_entity_decode($value);
        $data = str_replace("&lbrace;", "{", $data);
        $data = str_replace("&rbrace;", "}", $data);
        $data = json_decode($data, true);


        foreach ($data as $item) {
            $html .= '<li><a href="'. $item['afbeelding'] .'" target="_blank">'. $item['afbeelding'] .'</a></li>';
            # code...
        }
        return $html;
    }
}