title: Biografie
header_afbeelding: /assets/d85_4452-(large)-1544860169.JPG
biografieen:
  -
    type: biografie
    naam: 'Hans Kienhuis (gitaar en Zang)'
    bio: |
      Hans heeft met zijn 35 jaar ervaring als professioneel gitarist inmiddels zijn sporen in het muziekcircuit ruim verdiend. Hij speelt niet alleen gitaar maar neemt ook de backing vocals voor zijn rekening. Hij is vaardig binnen verschillende genres: van rock, blues, jazz tot pop en natuurlijk ….The Cats.
      Naast het runnen van zijn eigen gitaarschool, bouwt hij zijn eigen gitaren. Hiermee weet hij altijd weer een gevoelige snaar te raken! Professionaliteit staat bij hem altijd voorop, maar een goede sfeer mag natuurlijk nooit ontbreken. Deze combinatie en zijn passie voor muziek zorgen ervoor dat voor hem elk optreden één is om nooit te vergeten!
    profielfoto: /assets/hans-kienhuis.jpg
  -
    type: biografie
    naam: 'Emil Angouchev (slag/sologitaar)'
    bio: |
      Emil is geboren in Bulgarije, in het plaatsje Plovdiv. Hij speelde als beroepsmuzikant in meerdere internationale bands en werd regelmatig ingehuurd als studiomuzikant. Hij woont al jaren in Rheine (Duitsland) en is werkzaam als gitaardocent.
      Emil is een gevoelig mens en onze eigen ‘knuffelbeer’. Hij heeft inmiddels de Cats-muziek leren kennen en is er van gaan houden. Net als van de band, want Emil voelt zich thuis tussen zijn muziekvrienden van The Cats in the House. Zoals hij zelf zegt “Ihr seit meine neue Familie” én hij houdt van jullie, onze fans en publiek.
    profielfoto: /assets/emil04-(large).jpg
  -
    type: biografie
    naam: 'Edwin Peters (drums en zang)'
    bio: 'Edwin (1963) is geboren met drumstokken in zijn handjes, althans dat zegt ie zelf. Z’n moeder werd boos als de zoveelste emmer bezweek onder het geweld van haar kleine trommelende mannetje. Toen toch maar een drumstelletje gekocht. Op z’n 10 e begon Edwin te roffelen bij het plaatselijke drumkorps en daarna bij de big band. Vanaf zijn 15e speelt Edwin, bij vrienden ook wel bekend als “Mr. P”, in verschillende bands, totdat Frontpage werd opgericht. Met deze semi-professionele band trad hij vaak op, in Nederland en Duitsland, samen met André Bruns en Ruud Bittink. Naast The Cats in the House, speelt Edwin in de rockband Disorder en begeleidt hij gospelkoor Spirit. Net als Phil Collins en Herman van Boeyen, beheerst Edwin de combinatie van drummen en zingen tegelijk.Ook Edwin moest even wennen aan de muziek van The Cats, maar inmiddels wil hij samen met deze geweldige muziekvrienden de Cats-muziek nieuw leven inblazen.'
    profielfoto: /assets/edwin02-(large).jpg
  -
    type: biografie
    naam: 'René van der Veen (leadzang)'
    bio: |
      René (1966) speelde op jonge leeftijd al stemband; hij zong als kind fanatiek. Dat heeft ie nooit meer losgelaten en werd uiteindelijk zanger van verschillende bands. Anodyne, Bucks and Booze, Rio, Wolf to the Moon gingen vooraf aan The Cats in the House.
      René is, net als velen uit die tijd, opgegroeid met muziek van The Cats. Ook bij hem thuis werd deze muziek veel gedraaid. De lievelingsmuziek van René is naast The Cats, Classick Rock en natuurlijk Elvis Presley. Als een goed lijkende Elvis heeft ie veel opgetreden; kwam er in Duitsland mee op tv en trad ook op in België. Hoewel hij het Elvis-repertoire meesterlijk en vooral gevoelig zingt, is René op z’n best als zichzelf, bij The Cats in the House. Geen imitatie van Piet, Cees of Jaap, maar het eigen geluid van René van der Veen. Nederland heeft er weer een topzanger bij.
    profielfoto: /assets/rene02-(large).JPG
  -
    type: biografie
    naam: 'André Bruns (bas en zang)'
    bio: 'André (1963) begon zijn muzikale loopbaan op negenjarige leeftijd als trompettist, bij de plaatselijke fanfare. Op zijn veertiende stapte hij over naar de gitaar en begon hij met drummer Edwin Peters een band. Omdat ze geen bassist konden vinden, besloot André de bas om zijn nek te hangen en die heeft hij nooit meer afgedaan. Later kwam Ruud Bittink bij deze band en zo ontstond Frontpage, een semi-professionele band waarmee ze Nederland en Duitsland veroverden. Na een aantal jaren is André de grens overgestoken en speelde hij zes jaar lang, week in week uit, in verschillende bands. Voor hem is met The Cats in the House een periode van “relaxen” aangebroken. Niets moet meer; alles is voor de muziek en voor het plezier dat de band heeft en geeft. In het dagelijks leven is André werkzaam als chauffeur op een zorg ambulance bij een Duitse organisatie.'
    profielfoto: /assets/andre03-(large).jpg
  -
    type: biografie
    naam: 'Sander Veldhuis (toetsen en  zang)'
    bio: |
      Sander (1970) kroop als peuter al op de pianokruk om de piano te ontdekken. Met 11 jaar begon hij serieus muziek te maken op een electronisch orgel (helaas was de piano een paar jaar daarvoor ingeruild). Op zijn 16e had hij zijn eerste synthesizer bij elkaar gespaard, er zouden er nog vele volgen. Vanaf die tijd ging hij ook in verschillende bandjes spelen.  Naast The Cats in the House speelt hij toetsen bij Johnny Teardrop en het James Last Revival Orchestra. Hij wordt ook ingezet bij verschillende projecten waar hij een aantal bekende artiesten heeft mogen begeleiden.
      Sander heeft een brede muzikale smaak waarbij de voorkeur uitgaat naar muziek van de jaren 70 en 80. De bijzonder herkenbare sound van The Cats spreekt hem aan.
      Op de bühne spelen geeft hem een geweldige kick, helemaal als er goed wordt gespeeld en het geluid goed is. En het is echt af als het publiek heeft genoten.
    profielfoto: /assets/sander.jpg
  -
    type: biografie
    naam: 'Ruud Bittink (toetsen en zang)'
    bio: 'Ruud (1966) speelt al sinds zijn 9e jaar serieus piano. Dat heeft hij niet van een vreemde, want zijn moeder is ook een uitstekende pianiste. Zijn dochter Elsemarie zet deze traditie voort, naast haar werk voor onze fanshop. Ruud heeft zijn piano-opleiding genoten bij Martin Kaptijn en later aan de Eberhardt Jazz Schule in Hamburg. Ambitieus en gedreven, dat is kenmerkend voor Ruud. Hij is ook de grondlegger van The Cats in the House en heeft de band samengesteld. Sinds zijn 17e speelt hij in bands op semi-professioneel niveau. Ruud kent onze drummer Edwin en bassist André al heel lang uit andere bands; hij speelde o.a. in Frontpage, Cabin and the Farts, Bucks and Booze en Blue Avenue. Zijn favoriete bands zijn Supertramp en Vaya Con Dios, “omdat het eerlijke muziek is zonder overstuurde gitaren en met mooie melodielijnen. Dat hebben The Cats eigenlijk ook: symfonische keyboardpartijen met akoestisch gitaarwerk.” Voor Ruud ligt de lat hoog en is discipline een voorwaarde om de Cats-muziek goed over de bühne te brengen.'
    profielfoto: /assets/ruud01-(large).jpg
  -
    type: biografie
    naam: 'Wim Bussink (trompet, percussionist en zang)'
    bio: |
      Wim Bussink  (trompet, percussionist en zang)
      Wim speelt vanaf zijn 9e trompet. Begonnen bij de plaatselijke fanfare en daarna al snel in meerdere orkesten waaronder Die Dinkelländer,  Streetlife, Bucks and Booze en diverse begeleidingsorkesten. Zijn dienstplicht vervulde hij als trompettist bij de militaire kapel van de Artillerie. Tegenwoordig speelt Wim in enkele begeleidingsorkesten en omdat hij het toch niet laten kan recent bij ons, The Cats in the House. Naast trompet en bugel verzorgt Wim tevens de percussie en de background zang.
    profielfoto: /assets/d85_5258-(small).JPG
  -
    type: biografie
    naam: 'Harold Wolbert (manager)'
    bio: 'Harold (1975) is mede-eigenaar van een bouwbedrijf, getrouwd en heeft drie kinderen. Dat ie kan bouwen bewijst hij ook bij The Cats in the House. Muzikanten zijn vaak gevoelige en eigenwijze types, die allemaal hun opvatting hebben over hoe het moet. Harold leidt dat bij ons in goede banen; niet alleen het boeken van optredens, maar ook als gastheer van onze repetitieruimte, de heerlijke koffie en gevulde koelkast. Harold regelt alles wat nodig is voor een geslaagd optreden. Ooit wou hij drummer worden, maar daar is het niet van gekomen. Wel heeft hij jarenlang een eigen geheime zender gehad waar hij zijn favoriete Nederlandstalige en Golden Oldies-muziek draaide. Harold kan het hele Cats-repertoire dromen en meezingen. Muziek van The Cats raakt hem en dat maakt Harold de perfecte steun en toeverlaat voor de muzikanten, ook in muzikaal opzicht. Harold heeft er zin in: “Ik ben door de band in een hele andere wereld terechtgekomen en dat vind ik fascinerend!”'
    profielfoto: /assets/harold-wolbert-(large).jpg
  -
    type: biografie
    naam: 'Mirco Veldboer (fotografie)'
    bio: |
      Mirco (1969) is onze eigen “hoffotograaf” en al sinds 12 jaar groepsleider binnen een instelling voor verstandelijk beperkte kinderen. Daarnaast is hij semi professioneel fotograaf en gespecialiseerd in concerten, portretten, evenementen, huwelijken en opdrachten voor bedrijven. 
      Als kind kwam hij al vroeg in aanraking met The Cats, bijna letterlijk, want zijn ouders hadden een restaurant waar The Cats altijd aten, voordat ze in Denekamp optraden. Zijn vader had The Cats op de bandrecorder (en later op cassettebandjes) staan, dus het is Mirco met de paplepel ingegoten. Toen hij werd gevraagd om de optredens van The Cats in the House op beeld vast te leggen was hij vereerd. Door de mix van de verschillende persoonlijkheden is er volgens Mirco een mooie combinatie ontstaan van allemaal vakmensen. Mirco hoopt dat The Cats in the House  Nederland gaat veroveren, maar zoals het er nu naar uitziet zou dat nog wel eens eerder kunnen gaan gebeuren dan we denken. Daarnaast kijkt hij nu al uit naar de eerste theatertour.
    profielfoto: /assets/mirco-in-action-(large).jpg
  -
    type: biografie
    naam: 'Sanne en Elsemarie (fanshop)'
    bio: 'Sanne Bruns, dochter van André, en Elsemarie, dochter van Ruud, zijn al vanaf het eerste uur betrokken bij de band. Niet alleen als dochters die hun vaders bewonderen, maar ook al snel als beheerders van onze fanshop. Ze zijn verantwoordelijk voor in- en verkoop van sjaals, zonnebrillen, polsbandjes, t-shirts, etc. Deze stoere meiden zijn ieder optreden aanwezig en helpen u graag met passen. Na ieder optreden krijgt de band ongezouten kritiek over alles wat wel, maar ook wat niet goedging. Waar zouden we zijn zonder onze dochters?'
    profielfoto: /assets/dames-(large).jpg
  -
    type: biografie
    naam: 'Melvin de Winter ( MDW geluid en licht)'
    bio: 'Melvin (1993) is geboren tussen geurige krentenwegge, volkorenbrood en tompoezen. Als kruimel uit een bakkersgezin, begon hij op zijn 16e met zijn bedrijf MDW Geluid & Licht. Melvin heeft een technische achtergrond en volgde zij opleiding bij het Instituut voor Audio- en Belichtingstechniek. De ervaring om iets moois te maken van geluid en licht, drijft Melvin en daar geniet hij iedere keer van. The Cats in the House is voor Melvin een enorme uitdaging: “een gezellige groep goede muzikanten, die je bij elk optreden ziet groeien. Dat ik daar mijn steentje aan bij mag dragen, vind ik geweldig!” Helaas voor de muzikanten, moeten zij het ieder optreden stellen zonder de ervaring van het geluid in de zaal. Gelukkig geniet het publiek er met volle teugen van en, afgaand op de reacties van de kenners, zet Melvin een topprestatie neer.'
    profielfoto: /assets/melvin-voor-website-(large).jpg
template: biografie
fieldset: biografie
id: 35806153-e514-4f78-ad32-ea6629b05b54
