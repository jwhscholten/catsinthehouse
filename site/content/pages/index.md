---
hero_image: /assets/42413602_2197993300419818_6426422572727926784_o.jpg
intro_image: /assets/photo-2018-11-29-17-58-06.jpg
intro_title: 'The Cats in the House'
intro_tekst: |
  Welkom op de website van de Twentse formatie “The Cats in the House”. Een professionele band die bestaat uit acht topmuzikanten. 
  Wat in 2017 begon als een eenmalige stunt, is inmiddels uitgegroeid tot een serieus muzikaal project. Belangrijke ex The Cats-leden als Piet Veerman en Arnold Mühren zijn blij met de komst van “The Cats in the House”. Niet alleen omdat zij de Cats-muziek vertolken, maar ook omdat zich inmiddels een eigen geluid ontwikkelt dat volgens kenners in de buurt komt van de sfeer en sound van de originele Cats. Kenmerkend voor “The Cats in the House”-sound zijn, naast samenzang en een geweldige leadzanger, hun stevige ritmesectie en het gebruik van blaasinstrumenten. 
  
  Hier naast een foto van de originele bezetting van The Cats met  Cees en Piet Veerman, Jaap Schilder, Theo Klouwer en Arnold Mühren, de vaderlandse topband uit de jaren 1964 en 1985.
title: Home
template: home
fieldset: home
posts: 3
id: db0ae4e3-4f10-4802-bc40-0b880cbf02c7
---
Our latest blog posts: