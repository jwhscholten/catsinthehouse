<?php

namespace Statamic\Addons\FormHandler;

use Statamic\Extend\Tags;

class FormHandlerTags extends Tags
{
    /**
     * The {{ form_handler }} tag
     *
     * @return string|array
     */
    public function index()
    {
        //
    }

    /**
     * The {{ form_handler:example }} tag
     *
     * @return string|array
     */
    public function example()
    {
        //
    }
}
