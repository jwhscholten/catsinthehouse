var cart_cookie = null;

function slugify(text) {
	return text
		.toString()
		.toLowerCase()
		.replace(/\s+/g, "-") // Replace spaces with -
		.replace(/[^\w\-]+/g, "") // Remove all non-word chars
		.replace(/\-\-+/g, "-") // Replace multiple - with single -
		.replace(/^-+/, "") // Trim - from start of text
		.replace(/-+$/, ""); // Trim - from end of text
}

$(document).ready(function() {
	var cartItems = JSON.parse(localStorage.getItem("cart"));
	var price = 0;

	if (cartItems && cartItems.length) {
		
		$(".cart__badge").text(cartItems.length);
		$(".cart__badge").show();
		$(".input--bestelling").val(JSON.stringify(cartItems));

		cartItems.forEach(function(value) {
			$(".order--list").append(
				'<div class="col-3 thumb"><img class="img-fluid mb-3" src=' +
					value.afbeelding +
					'/><button type="button" class="btn btn-sm remove-item text-white" data-url=' +
					value.naam +
					'> <i class="fas fa-shopping-cart text-white"></i><span class="delete">Verwijderen</span></button></div>'
			);

			

			$('button[data-url="'+ value.naam +'"]').addClass('active');

		});

		cartItems.forEach(function(value) {

			price += parseFloat(value.price);
		});
		$(".order--price").text("€ " + price.toFixed(2));
		$(".input--price").val(price.toFixed(2));
	} else {
		$(".order--list").append(
			'<h6 class="pl-4">Uw winkelmandje is leeg</h6>'
		);
	}

	//DOM manipulation code
	// cart_cookie = Cookies.get('cart');
	// console.log(cart_cookie)
});

$(".add-to-cart").click(function(e) {
	e.preventDefault();
	e.stopPropagation();

	$(this).toggleClass("active");

	var cartData = JSON.parse(localStorage.getItem("cart")) || [];
	var $this = $(this);

	var item = {
		concert: slugify($(".title--bordered").text()),
		naam: $this.data("url"),
		price: $this.data("price"),
		afbeelding: "https://thecatsinthehouse.nl/" + $this.data("url")
	};

	var cartIndex = cartData.findIndex(i => i.naam === item.naam);

	if (cartIndex > -1) {
		cartData.splice(cartIndex, 1);
		if (cartData.length) {
			$(".cart__badge").show();
		} else {
			$(".cart__badge").hide();
		}
	} else {
		cartData.push(item);
		$(".cart__badge").show();
	}

	$(".cart__badge").text(cartData.length);
	localStorage.setItem("cart", JSON.stringify(cartData));
});

$(".order--list").on("click", ".remove-item", function(e) {
	e.preventDefault();
	e.stopPropagation();

	var cartData = JSON.parse(localStorage.getItem("cart"));
	var $this = $(this);
	var price = 0;

	var cartIndex = cartData.findIndex(i => i.naam === $this.data("url"));

	if (cartIndex > -1) {
		cartData.splice(cartIndex, 1);
	}
	$(".order--list").empty();
	$(".order--price").empty();
	$(".cart__badge").text(cartData.length);
	localStorage.setItem("cart", JSON.stringify(cartData));
	if (cartData.length) {
		cartData.forEach(function(value) {
			price += parseFloat(value.price);
		});
		$(".order--price").text("€" + price.toFixed(2));
		$(".input--price").val(price.toFixed(2));

		cartData.forEach(function(value) {
			$(".order--list").append(
				'<div class="col-3 thumb"><img class="img-fluid mb-3" src=' +
					value.afbeelding +
					'/><button type="button" class="btn btn-sm remove-item text-white" data-url=' +
					value.naam +
					'> <i class="fas fa-shopping-cart text-white"></i><span class="delete">Verwijderen</span></button></div>'
			);
		});
	} else {
		$(".order--list").append(
			'<h6 class="pl-4">Uw winkelmandje is leeg</h6>'
		);
	}
});
