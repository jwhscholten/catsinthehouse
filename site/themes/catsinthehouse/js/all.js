var cart_cookie = null;

function slugify(text) {
	return text
		.toString()
		.toLowerCase()
		.replace(/\s+/g, "-") // Replace spaces with -
		.replace(/[^\w\-]+/g, "") // Remove all non-word chars
		.replace(/\-\-+/g, "-") // Replace multiple - with single -
		.replace(/^-+/, "") // Trim - from start of text
		.replace(/-+$/, ""); // Trim - from end of text
}

$(document).ready(function() {
	var cartItems = JSON.parse(localStorage.getItem("cart"));
	var price = 0;

	if (cartItems && cartItems.length) {
		
		$(".cart__badge").text(cartItems.length);
		$(".cart__badge").show();
		$(".input--bestelling").val(JSON.stringify(cartItems));

		cartItems.forEach(function(value) {
			$(".order--list").append(
				'<div class="col-3 thumb"><img class="img-fluid mb-3" src=' +
					value.afbeelding +
					'/><button type="button" class="btn btn-sm remove-item text-white" data-url=' +
					value.naam +
					'> <i class="fas fa-shopping-cart text-white"></i><span class="delete">Verwijderen</span></button></div>'
			);

			

			$('button[data-url="'+ value.naam +'"]').addClass('active');

		});

		cartItems.forEach(function(value) {

			price += parseFloat(value.price);
		});
		$(".order--price").text("€ " + price.toFixed(2));
		$(".input--price").val(price.toFixed(2));
	} else {
		$(".order--list").append(
			'<h6 class="pl-4">Uw winkelmandje is leeg</h6>'
		);
	}

	//DOM manipulation code
	// cart_cookie = Cookies.get('cart');
	// console.log(cart_cookie)
});

$(".add-to-cart").click(function(e) {
	e.preventDefault();
	e.stopPropagation();

	$(this).toggleClass("active");

	var cartData = JSON.parse(localStorage.getItem("cart")) || [];
	var $this = $(this);

	var item = {
		concert: slugify($(".title--bordered").text()),
		naam: $this.data("url"),
		price: $this.data("price"),
		afbeelding: "https://thecatsinthehouse.nl/" + $this.data("url")
	};

	var cartIndex = cartData.findIndex(i => i.naam === item.naam);

	if (cartIndex > -1) {
		cartData.splice(cartIndex, 1);
		if (cartData.length) {
			$(".cart__badge").show();
		} else {
			$(".cart__badge").hide();
		}
	} else {
		cartData.push(item);
		$(".cart__badge").show();
	}

	$(".cart__badge").text(cartData.length);
	localStorage.setItem("cart", JSON.stringify(cartData));
});

$(".order--list").on("click", ".remove-item", function(e) {
	e.preventDefault();
	e.stopPropagation();

	var cartData = JSON.parse(localStorage.getItem("cart"));
	var $this = $(this);
	var price = 0;

	var cartIndex = cartData.findIndex(i => i.naam === $this.data("url"));

	if (cartIndex > -1) {
		cartData.splice(cartIndex, 1);
	}
	$(".order--list").empty();
	$(".order--price").empty();
	$(".cart__badge").text(cartData.length);
	localStorage.setItem("cart", JSON.stringify(cartData));
	if (cartData.length) {
		cartData.forEach(function(value) {
			price += parseFloat(value.price);
		});
		$(".order--price").text("€" + price.toFixed(2));
		$(".input--price").val(price.toFixed(2));

		cartData.forEach(function(value) {
			$(".order--list").append(
				'<div class="col-3 thumb"><img class="img-fluid mb-3" src=' +
					value.afbeelding +
					'/><button type="button" class="btn btn-sm remove-item text-white" data-url=' +
					value.naam +
					'> <i class="fas fa-shopping-cart text-white"></i><span class="delete">Verwijderen</span></button></div>'
			);
		});
	} else {
		$(".order--list").append(
			'<h6 class="pl-4">Uw winkelmandje is leeg</h6>'
		);
	}
});



$('.img-gallery a').simpleLightbox();



/*!
 * classie - class helper functions
 * from bonzo https://github.com/ded/bonzo
 * 
 * classie.has( elem, 'my-class' ) -> true/false
 * classie.add( elem, 'my-new-class' )
 * classie.remove( elem, 'my-unwanted-class' )
 * classie.toggle( elem, 'my-class' )
 */

/*jshint browser: true, strict: true, undef: true */
/*global define: false */

( function( window ) {

'use strict';

// class helper functions from bonzo https://github.com/ded/bonzo

function classReg( className ) {
  return new RegExp("(^|\\s+)" + className + "(\\s+|$)");
}

// classList support for class management
// altho to be fair, the api sucks because it won't accept multiple classes at once
var hasClass, addClass, removeClass;

if ( 'classList' in document.documentElement ) {
  hasClass = function( elem, c ) {
    return elem.classList.contains( c );
  };
  addClass = function( elem, c ) {
    elem.classList.add( c );
  };
  removeClass = function( elem, c ) {
    elem.classList.remove( c );
  };
}
else {
  hasClass = function( elem, c ) {
    return classReg( c ).test( elem.className );
  };
  addClass = function( elem, c ) {
    if ( !hasClass( elem, c ) ) {
      elem.className = elem.className + ' ' + c;
    }
  };
  removeClass = function( elem, c ) {
    elem.className = elem.className.replace( classReg( c ), ' ' );
  };
}

function toggleClass( elem, c ) {
  var fn = hasClass( elem, c ) ? removeClass : addClass;
  fn( elem, c );
}

var classie = {
  // full names
  hasClass: hasClass,
  addClass: addClass,
  removeClass: removeClass,
  toggleClass: toggleClass,
  // short names
  has: hasClass,
  add: addClass,
  remove: removeClass,
  toggle: toggleClass
};

// transport
if ( typeof define === 'function' && define.amd ) {
  // AMD
  define( classie );
} else {
  // browser global
  window.classie = classie;
}

})( window );

      (function() {
        // trim polyfill : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/Trim
        if (!String.prototype.trim) {
          (function() {
            // Make sure we trim BOM and NBSP
            var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
            String.prototype.trim = function() {
              return this.replace(rtrim, '');
            };
          })();
        }

        [].slice.call( document.querySelectorAll( 'input.input__field' ) ).forEach( function( inputEl ) {
          // in case the input is already filled..
          if( inputEl.value.trim() !== '' ) {
            classie.add( inputEl.parentNode, 'input--filled' );
          }

          // events:
          inputEl.addEventListener( 'focus', onInputFocus );
          inputEl.addEventListener( 'blur', onInputBlur );
        } );

        function onInputFocus( ev ) {
          classie.add( ev.target.parentNode, 'input--filled' );
        }

        function onInputBlur( ev ) {
          if( ev.target.value.trim() === '' ) {
            classie.remove( ev.target.parentNode, 'input--filled' );
          }
        }
      })();
$('.navbar-toggler').click(function(){

	$('nav').toggleClass('mobile-nav--visible')

});
//# sourceMappingURL=all.js.map
