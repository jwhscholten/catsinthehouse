<?php

namespace Statamic\Addons\FormHandler;

use Statamic\Extend\Listener;
use Statamic\Contracts\Forms\Submission;



class FormHandlerListener extends Listener
{


    /**
     * The events to be listened for, and the methods to call.
     *
     * @var array
     */
    public $events = ['Form.submission.creating' => 'handleForm'];


        public function handleForm($submission)
        {
            dd($submission);

        	return $submission;
        }

}
