<?php

namespace Statamic\Addons\FormHandler;

use Statamic\Extend\API;

class FormHandlerAPI extends API
{
    /**
     * Accessed by $this->api('FormHandler')->example() from other addons
     */
    public function example()
    {
        //
    }
}
