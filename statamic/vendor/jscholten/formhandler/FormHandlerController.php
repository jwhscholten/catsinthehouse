<?php

namespace Statamic\Addons\FormHandler;

use Statamic\Extend\Controller;

class FormHandlerController extends Controller
{
    /**
     * Maps to your route definition in routes.yaml
     *
     * @return mixed
     */
    public function index()
    {
        return $this->view('index');
    }
}
